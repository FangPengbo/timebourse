package com.timebourse.common.valid;

import com.sun.istack.internal.NotNull;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 自定义校验注解
 * 校验是否包含指定范围的值
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/04/26/23:20
 * @Description:
 */

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {ListValueConstraintValidator.class}
)
public @interface ListValue {


    //必须
    String message() default "{com.timebourse.common.valid.ListValue.message}";
    //必须
    Class<?>[] groups() default {};
    //必须
    Class<? extends Payload>[] payload() default {};

    //自定义参数
    int [] vals() default {};


}
