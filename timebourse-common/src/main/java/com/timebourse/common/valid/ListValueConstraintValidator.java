package com.timebourse.common.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义校验器
 * 校验指定值
 * 实现ConstraintValidator接口,泛型第一个为指定的自定义注解,第二个是要验证的类型
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/04/26/23:34
 * @Description:
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

  private Set<Integer> set =  new HashSet<>();

    /**
     * 初始化方法
     * @param constraintAnnotation 自定义注解传来的信息
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {

        int[] vals = constraintAnnotation.vals();
        for (int val:
             vals) {
            set.add(val);
        }
    }


    /**
     * 校验信息
     * @param value 字段值
     * @param context 上下文
     * @return
     */
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if(set.size() == 0 ) return true;
        return set.contains(value);
    }
}
