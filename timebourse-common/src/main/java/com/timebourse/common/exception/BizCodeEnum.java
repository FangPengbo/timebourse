package com.timebourse.common.exception;

/**
 * ERROR Exception Code And Message
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/04/21/23:19
 * @Description:
 */
public enum BizCodeEnum {

    VAILD_EXCEPTION(10001,"参数格式验证失败"),
    UNKNOWN_EXCEPTION(10000,"未知异常,请联系管理员");


    private int code;
    private String msg;

    BizCodeEnum(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
