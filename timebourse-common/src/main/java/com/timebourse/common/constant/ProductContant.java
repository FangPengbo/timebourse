package com.timebourse.common.constant;

/**
 * 商品相关的枚举类
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/04/14:12
 * @Description:
 */
public class ProductContant {

    public enum AttrEnum{

        ATTR_TYPE_BASE(1,"基本属性"),ATTR_TYPE_SALE(0,"销售属性");

        private int code;
        private String msg;

        AttrEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public int getCode() {
            return code;
        }
    }
}
