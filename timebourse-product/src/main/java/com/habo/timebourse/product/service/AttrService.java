package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.product.vo.AttrRespVo;
import com.habo.timebourse.product.vo.AttrVo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存attr时保存关联关系
     * @param attr
     */
    void saveAttr(AttrVo attr);

    /**
     * 分页查询所有商品属性
     * @param params
     * @param catelogId
     * @param attrType 请求类型
     * @return
     */
    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType);

    /**
     * 查询属性详细内容
     * @param attrId
     * @return
     */
    AttrRespVo getDetail(Long attrId);

    /**
     * 修改属性的时候级联修改关联关系
     * @param attrvo
     */
    void updateByattrvo(AttrRespVo attrvo);

    /**
     * 获取分组关联的属性
     * @param attrGroupId
     * @return
     */
    List<AttrEntity> getRelationAttr(Long attrGroupId);

    /**
     * 获取没有被当前分组关联的属性
     * @param params
     * @param attrGroupId
     * @return
     */
    PageUtils getNotRelationAttr(Map<String, Object> params, Long attrGroupId);
}

