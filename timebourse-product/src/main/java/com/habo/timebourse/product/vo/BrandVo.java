package com.habo.timebourse.product.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/06/23:32
 * @Description:
 */
@Data
public class BrandVo {

    Long brandId;
    String brandName;



}
