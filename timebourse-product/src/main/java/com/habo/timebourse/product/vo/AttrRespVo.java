package com.habo.timebourse.product.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * 属性分组响应
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/03/21:34
 * @Description:
 */
@Data
public class AttrRespVo extends AttrVo implements Serializable {

    private String catelogName;
    /**
     * 所属分组
     */
    private String groupName;

    /**
     * 三级分类完整路径
     */
    private Long[] catelogPath;

    /**
     * 分组ID
     */
    private Long attrGroupId;
}
