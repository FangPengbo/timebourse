package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.product.entity.BrandEntity;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新增品牌的关联关系
     * @param categoryBrandRelation
     */
    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);

    /**
     * 更新关联表中品牌的名字
     * @param brandId
     * @param name
     */
    void updateBrand(Long brandId, String name);

    /**
     * 更新关联表中分类的名字
     * @param catId
     * @param name
     */
    void upDateCategory(Long catId, String name);

    /**
     * 获取分类关联的品牌
     * @param catId
     * @return
     */
    List<BrandEntity> selectBrandsByCatId(Long catId);

}

