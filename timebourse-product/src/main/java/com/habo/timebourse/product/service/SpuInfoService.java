package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.product.vo.SpuSaveVo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:34
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存商品信息
     * @param spuvo
     */
    void saveSpuInfo(SpuSaveVo spuvo);

    PageUtils queryParamsPage(Map<String, Object> params);
}

