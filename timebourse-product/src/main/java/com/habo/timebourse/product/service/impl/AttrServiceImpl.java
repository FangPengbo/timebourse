package com.habo.timebourse.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.habo.timebourse.product.dao.AttrAttrgroupRelationDao;
import com.habo.timebourse.product.dao.AttrGroupDao;
import com.habo.timebourse.product.dao.CategoryDao;
import com.habo.timebourse.product.entity.AttrAttrgroupRelationEntity;
import com.habo.timebourse.product.entity.AttrGroupEntity;
import com.habo.timebourse.product.entity.CategoryEntity;
import com.habo.timebourse.product.service.CategoryService;
import com.habo.timebourse.product.vo.AttrRespVo;
import com.habo.timebourse.product.vo.AttrVo;
import com.timebourse.common.constant.ProductContant;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.AttrDao;
import com.habo.timebourse.product.entity.AttrEntity;
import com.habo.timebourse.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AttrDao attrDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void saveAttr(AttrVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.save(attrEntity);
        //如果是销售属性则不用新增关联表信息
        if (attr.getAttrType() == ProductContant.AttrEnum.ATTR_TYPE_BASE.getCode() && attr.getAttrGroupId() != null) {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttrGroupId(attr.getAttrGroupId());
            attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());
            attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);
        }
    }

    @Override
    public PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType) {
        QueryWrapper<AttrEntity> queryWrapper = new QueryWrapper<AttrEntity>()
                .eq("attr_type", "base".equalsIgnoreCase(attrType)
                        ? ProductContant.AttrEnum.ATTR_TYPE_BASE.getCode()
                        : ProductContant.AttrEnum.ATTR_TYPE_SALE.getCode());
        //精准查询
        if (catelogId != 0) {
            queryWrapper.eq("catelog_id", catelogId);
        }
        //查询条件
        String key = (String) params.get("key");
        //模糊查询
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.and((wrapper) -> {
                wrapper.eq("attr_id", key).or().like("attr_name", key);
            });
        }

        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                queryWrapper
        );

        //构建返回体
        List<AttrEntity> records = page.getRecords();

        List<AttrRespVo> list = records.stream().map(el -> {
            AttrRespVo attrRespVo = new AttrRespVo();
            //copy attr -> attrvo
            BeanUtils.copyProperties(el, attrRespVo);
            //如果是销售属性,则不用设置分组内容
            if ("base".equalsIgnoreCase(attrType)) {
                //set groupName
                AttrAttrgroupRelationEntity relationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", el.getAttrId()));
                if (relationEntity != null && relationEntity.getAttrGroupId() != null) {
                    AttrGroupEntity groupEntity = attrGroupDao.selectById(relationEntity.getAttrGroupId());
                    attrRespVo.setGroupName(groupEntity.getAttrGroupName());
                }
            }
            //set catelogName
            CategoryEntity categoryEntity = categoryDao.selectById(el.getCatelogId());
            if (categoryEntity != null) {
                attrRespVo.setCatelogName(categoryEntity.getName());
            }
            return attrRespVo;
        }).collect(Collectors.toList());

        //更换返回体
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(list);

        return pageUtils;

    }

    @Override
    public AttrRespVo getDetail(Long attrId) {
        AttrRespVo attrRespVo = new AttrRespVo();
        //select attr
        AttrEntity attrEntity = this.baseMapper.selectById(attrId);
        //copy attr -> attrvo
        BeanUtils.copyProperties(attrEntity, attrRespVo);

        //set 回显所属分类
        Long[] catelogIds = categoryService.findCatelogIds(attrEntity.getCatelogId());
        attrRespVo.setCatelogPath(catelogIds);

        //set 回显所属分组
        AttrAttrgroupRelationEntity relationEntity = attrAttrgroupRelationDao.selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId()));
        if (relationEntity != null) {
            attrRespVo.setAttrGroupId(relationEntity.getAttrGroupId());
        }

        return attrRespVo;
    }

    @Transactional
    @Override
    public void updateByattrvo(AttrRespVo attrvo) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attrvo, attrEntity);
        this.updateById(attrEntity);
        //如果是销售属性,则不用更新关联表中的信息
        if (attrvo.getAttrType() == ProductContant.AttrEnum.ATTR_TYPE_BASE.getCode()) {
            //update relation
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrGroupId(attrvo.getAttrGroupId());
            relationEntity.setAttrId(attrvo.getAttrId());

            Integer attr_id = attrAttrgroupRelationDao.selectCount(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrvo.getAttrId()));
            if (attr_id > 0) {
                attrAttrgroupRelationDao.update(relationEntity, new UpdateWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrvo.getAttrId()));
            } else {
                attrAttrgroupRelationDao.insert(relationEntity);
            }
        }
    }

    @Override
    public List<AttrEntity> getRelationAttr(Long attrGroupId) {
        //获取到所有关联实体
        List<AttrAttrgroupRelationEntity> entities = attrAttrgroupRelationDao.selectList(new QueryWrapper<AttrAttrgroupRelationEntity>()
                .eq("attr_group_id", attrGroupId));
        if(entities == null || entities.size() == 0){
            return null;
        }
        //抽取实体中的attrid
        List<Long> attrIds = entities.stream().map(el -> {
            return el.getAttrId();
        }).collect(Collectors.toList());

        //查询所有AttrEntity
        List<AttrEntity> attrEntityList = attrDao.selectBatchIds(attrIds);
        return attrEntityList;
    }

    @Override
    public PageUtils getNotRelationAttr(Map<String, Object> params, Long attrGroupId) {
        //当前分组只能关联自己所属的分类里边的所有属性
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrGroupId);
        Long catelogId = attrGroupEntity.getCatelogId();
        //当前分组只能关联别的分组没有引用的属性

        //抽取当前分类下的分组信息
        List<AttrGroupEntity> attrGroupEntities = attrGroupDao.selectList(new QueryWrapper<AttrGroupEntity>()
                .eq("catelog_id", catelogId));
        //抽取可关联的分组id
        List<Long> attrGroupIds = attrGroupEntities.stream().map(el -> {
            return el.getAttrGroupId();
        }).collect(Collectors.toList());

        //查询已经被关联的基本属性
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = attrAttrgroupRelationDao.selectList(
                new QueryWrapper<AttrAttrgroupRelationEntity>()
                .in("attr_group_id", attrGroupIds));
        //抽取已经关联的基本属性Id
        List<Long> AttrIds = attrAttrgroupRelationEntities.stream().map(el -> {
            return el.getAttrId();
        }).collect(Collectors.toList());

        //构造查询条件
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<AttrEntity>()
                .eq("catelog_id", catelogId)
                .eq("attr_type",ProductContant.AttrEnum.ATTR_TYPE_BASE.getCode());

        if(AttrIds != null && AttrIds.size() > 0){
                wrapper.notIn("attr_id", AttrIds);
        }
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.and(w ->{
                w.eq("attr_id",key).or().like("attr_name",key);
            });
        }

        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);
        PageUtils pageUtils = new PageUtils(page);
        return pageUtils;
    }

}