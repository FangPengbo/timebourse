package com.habo.timebourse.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.habo.timebourse.product.feign")
@EnableDiscoveryClient
@MapperScan("com.habo.timebourse.product.dao")
@SpringBootApplication
public class TimebourseProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseProductApplication.class, args);
    }

}
