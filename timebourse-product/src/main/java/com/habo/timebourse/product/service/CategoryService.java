package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenusByIds(List<Long> asList);

    /**
     * 找到三级分类完整路径
     * @param catelogId
     * @return
     */
    Long[] findCatelogIds(Long catelogId);

    /**
     * 级联更新 分类与品牌分类关联
     * @param category
     */
    void updateDetail(CategoryEntity category);
}

