package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 级联更新
     * @param brand
     */
    void updateDetail(BrandEntity brand);
}

