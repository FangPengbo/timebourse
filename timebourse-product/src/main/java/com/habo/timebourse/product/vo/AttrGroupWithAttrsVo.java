package com.habo.timebourse.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.habo.timebourse.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/07/21:46
 * @Description:
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AttrGroupWithAttrsVo {

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;


    /**
     * 分组下所有属性
     */
    List<AttrEntity> attrs;

}
