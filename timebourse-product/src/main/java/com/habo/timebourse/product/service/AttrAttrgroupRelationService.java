package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.product.vo.AttrGroupRelationVo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.AttrAttrgroupRelationEntity;

import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 批量删除品牌关联属性
     * @param relations
     */
    void deleteRelations(AttrGroupRelationVo[] relations);

    /**
     * 添加品牌关联属性
     * @param relations
     */
    void saveRelations(AttrGroupRelationVo[] relations);

}

