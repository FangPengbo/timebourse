package com.habo.timebourse.product.service.impl;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.SkuInfoDao;
import com.habo.timebourse.product.entity.SkuInfoEntity;
import com.habo.timebourse.product.service.SkuInfoService;
import org.springframework.util.StringUtils;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SkuInfoEntity> wrapper = new QueryWrapper<>();

        //key为id or name
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.and(el -> {
                el.eq("sku_id",key)
                        .or()
                        .like("sku_name",key);
            });
        }

        //brandId 品牌
        String brandId = (String) params.get("brandId");
        if(!StringUtils.isEmpty(brandId)){
            BigDecimal bigDecimal = new BigDecimal(brandId);
            if(bigDecimal.compareTo(BigDecimal.ZERO) == 1){
                wrapper.eq("brand_id",brandId);
            }
        }
        //catelogId 分类
        String catelogId = (String) params.get("catelogId");
        if(!StringUtils.isEmpty(catelogId)){
            BigDecimal bigDecimal = new BigDecimal(catelogId);
            if(bigDecimal.compareTo(BigDecimal.ZERO) == 1){
                wrapper.eq("catalog_id",catelogId);
            }
        }

        //min 最低金额
        String min = (String) params.get("min");
        if(!StringUtils.isEmpty(min)){
                wrapper.ge("price",min);
        }
        //max 最大金额
        String max = (String) params.get("max");
        if(!StringUtils.isEmpty(max)){
            BigDecimal bigDecimal = new BigDecimal(max);
            if(bigDecimal.compareTo(BigDecimal.ZERO) == 1){
                wrapper.le("price",max);
            }
        }

        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);

    }

}