package com.habo.timebourse.product.vo;

import lombok.Data;

/**
 * 属性分组与属性关联关系
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/04/15:29
 * @Description:
 */
@Data
public class AttrGroupRelationVo {

    private Long attrId;
    private Long attrGroupId;

}
