package com.habo.timebourse.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.product.vo.AttrGroupWithAttrsVo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.product.entity.AttrGroupEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    /**
     * 获取分类下所有分组以及分组关联的属性
     * @param catelogId
     * @return
     */
    List<AttrGroupWithAttrsVo> getGroupToAttrsBycatelogId(Long catelogId);
}

