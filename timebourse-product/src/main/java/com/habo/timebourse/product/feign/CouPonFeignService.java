package com.habo.timebourse.product.feign;

import com.timebourse.common.to.SkuReductionTo;
import com.timebourse.common.to.SpuBoundTo;
import com.timebourse.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 远程调用优惠服务
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/11/22:32
 * @Description:
 */

@FeignClient("timebourse-coupon")
public interface CouPonFeignService {


    /**
     * 保存积分信息
     */
    @PostMapping("/coupon/spubounds/save")
    R saveBounds(@RequestBody SpuBoundTo spuBounds);


    /**
     * 保存打折信息
     * @param skuReductionTo
     * @return
     */
    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuReduction(SkuReductionTo skuReductionTo);
}
