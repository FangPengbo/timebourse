package com.habo.timebourse.product.dao;

import com.habo.timebourse.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:35
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
