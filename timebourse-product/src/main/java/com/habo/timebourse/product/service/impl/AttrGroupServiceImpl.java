package com.habo.timebourse.product.service.impl;

import com.habo.timebourse.product.entity.AttrEntity;
import com.habo.timebourse.product.service.AttrService;
import com.habo.timebourse.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.AttrGroupDao;
import com.habo.timebourse.product.entity.AttrGroupEntity;
import com.habo.timebourse.product.service.AttrGroupService;
import org.springframework.util.StringUtils;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private AttrService attrService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        //catelogId为0时查询所有数据
        IPage<AttrGroupEntity> page = null;
        if (catelogId == 0) {
            page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    new QueryWrapper<AttrGroupEntity>()
            );
        } else {
            String key = (String) params.get("key");
            QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId);
            if (!StringUtils.isEmpty(key)) {
                wrapper.and((obj) -> {
                    obj.eq("attr_group_id", key).or().like("attr_group_name", key);
                });
            }
            page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    wrapper);
        }
        return new PageUtils(page);
    }

    @Override
    public List<AttrGroupWithAttrsVo> getGroupToAttrsBycatelogId(Long catelogId) {
        //获取分类下所有分组
        List<AttrGroupEntity> attrGroupEntities = this.list(
                new QueryWrapper<AttrGroupEntity>()
                        .eq("catelog_id", catelogId));
        //获取分组下所有属性
        List<AttrGroupWithAttrsVo> attrGroupWithAttrsVos = attrGroupEntities.stream().map(el -> {
            AttrGroupWithAttrsVo withAttrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(el, withAttrsVo);
            withAttrsVo.setAttrs(attrService.getRelationAttr(el.getAttrGroupId()));
            return withAttrsVo;
        }).collect(Collectors.toList());

        return attrGroupWithAttrsVos;
    }

}