package com.habo.timebourse.product.service.impl;

import com.habo.timebourse.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.CategoryDao;
import com.habo.timebourse.product.entity.CategoryEntity;
import com.habo.timebourse.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 查询分类列表,以父子结构组装
     *
     * @return
     */
    @Override
    public List<CategoryEntity> listWithTree() {
        //查出所有分类
        List<CategoryEntity> all = baseMapper.selectList(null);
        //给每个菜单元素添加父节点
        //all.stream().forEach(el -> setParent(el, all));
        //获取一级元素
        List<CategoryEntity> collect = all.stream().filter(categoryEntity -> categoryEntity.getParentCid() == 0).collect(Collectors.toList());
        //查出一级分类递归组装树状结构
        List<CategoryEntity> roots = all.stream().filter(categoryEntity ->
                categoryEntity.getParentCid() == 0
        ).map((el) -> {
            el.setChildren(getChilrld(el, all));
            return el;
        }).sorted((el1, el2) -> {
            return (el1.getSort() == null ? 0 : el1.getSort()) - (el2.getSort() == null ? 0 : el2.getSort());
        }).collect(Collectors.toList());
        return roots;
        //return collect;
    }

    /**
     * 编写批量逻辑删除分类
     *
     * @param asList 分类id集合
     */
    @Override
    public void removeMenusByIds(List<Long> asList) {
        //TODO 要判断是否被引用

        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogIds(Long catelogId) {
        List<Long> ids = new ArrayList<>();

        CategoryEntity entity = this.getById(catelogId);

        if(entity.getParentCid() != 0){
            //递归寻找父节点
            ids = findParentPath(entity,ids);
        }
        Collections.reverse(ids);
        return ids.toArray(new Long[ids.size()]);
    }

    @Transactional
    @Override
    public void updateDetail(CategoryEntity category) {
        this.updateById(category);
        if(!StringUtils.isEmpty(category.getName())){
            categoryBrandRelationService.upDateCategory(category.getCatId(),category.getName());
        }

    }

    /**
     * 递归寻找父节点
     * @return
     */
    private List<Long> findParentPath(CategoryEntity entity,List<Long> ids){
        ids.add(entity.getCatId());
        if (entity.getParentCid() != 0){
            findParentPath(this.getById(entity.getParentCid()),ids);
        }
        return ids;
    }


    /**
     * 递归寻找每个节点的子节点
     *
     * @param root 当前节点
     * @param all  所有节点
     * @return
     */
    private List<CategoryEntity> getChilrld(CategoryEntity root, List<CategoryEntity> all) {
        if (root.getCatLevel().equals(new Integer(3))) return null;
        List<CategoryEntity> collect = all.stream().filter((el) -> {
            return el.getParentCid().equals(root.getCatId());
        }).map((el) -> {
            el.setChildren(getChilrld(el, all));
            return el;
        }).sorted((el1, el2) -> {
            return (el1.getSort() == null ? 0 : el1.getSort()) - (el2.getSort() == null ? 0 : el2.getSort());
        }).collect(Collectors.toList());
        return collect;
    }

    /*    *//**
     * 添加父元素
     *
     * @param entity
     *//*
    private void setParent(CategoryEntity entity, List<CategoryEntity> all) {
        //一级元素
        if (entity.getParentCid() == 0) return;
        //获取父元素
        Optional<CategoryEntity> first = all.stream().filter(el -> (long) el.getCatId() == (long) entity.getParentCid()).findFirst();
        if (first == null || !first.isPresent()) return;
        //获取父元素的子集并添加
        if (first.get().getChirlds() == null) {
            first.get().setChirlds(new ArrayList<CategoryEntity>());
        }
        first.get().getChirlds().add(entity);
    }*/


}