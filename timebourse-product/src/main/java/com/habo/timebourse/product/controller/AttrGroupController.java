package com.habo.timebourse.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.habo.timebourse.product.entity.AttrEntity;
import com.habo.timebourse.product.service.AttrAttrgroupRelationService;
import com.habo.timebourse.product.service.AttrService;
import com.habo.timebourse.product.service.CategoryService;
import com.habo.timebourse.product.vo.AttrGroupRelationVo;
import com.habo.timebourse.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.habo.timebourse.product.entity.AttrGroupEntity;
import com.habo.timebourse.product.service.AttrGroupService;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.R;



/**
 * 属性分组
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:26:10
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;


    /**
     * 获取分类下所有分组以及分组关联的属性
     */
    @GetMapping("/{catelogId}/withattr")
    public R withattr(@PathVariable("catelogId") Long catelogId){

        List<AttrGroupWithAttrsVo> attrsVoList = attrGroupService.getGroupToAttrsBycatelogId(catelogId);

        return R.ok().put("data",attrsVoList);
    }

    /**
     * 新增分组与属性关联关系
     */
    @PostMapping("/attr/relation")
    public R save(@RequestBody AttrGroupRelationVo[] relations){
        attrAttrgroupRelationService.saveRelations(relations);

        return R.ok();
    }


    /**
     * 批量删除分组关联属性
     */
    @PostMapping("/attr/relation/delete")
    public R delete(@RequestBody AttrGroupRelationVo[] relations){

        attrAttrgroupRelationService.deleteRelations(relations);

        return R.ok();
    }

    /**
     * 查出分组关联没有被关联的属性
     * @return
     */
    @GetMapping("/{attrGroupId}/noattr/relation")
    public R Notlist(@RequestParam Map<String, Object> params,
                  @PathVariable("attrGroupId") Long attrGroupId){

        PageUtils page = attrService.getNotRelationAttr(params,attrGroupId);

        return R.ok().put("page", page);
    }


    /**
     * 查出分组关联的属性
     * @return
     */
    @GetMapping("/{attrGroupId}/attr/relation")
    public R list(@PathVariable("attrGroupId") Long attrGroupId){
        List<AttrEntity> entities = attrService.getRelationAttr(attrGroupId);

        return R.ok().put("data",entities);
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,
                    @PathVariable("catelogId") Long catelogId){
        //PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params,catelogId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long catelogId = attrGroup.getCatelogId();

        Long[] catelogIds = categoryService.findCatelogIds(catelogId);

        attrGroup.setCatelogPath(catelogIds);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
