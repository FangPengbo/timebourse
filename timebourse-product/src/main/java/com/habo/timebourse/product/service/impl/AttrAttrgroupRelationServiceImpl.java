package com.habo.timebourse.product.service.impl;

import com.habo.timebourse.product.vo.AttrGroupRelationVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.AttrAttrgroupRelationDao;
import com.habo.timebourse.product.entity.AttrAttrgroupRelationEntity;
import com.habo.timebourse.product.service.AttrAttrgroupRelationService;


@Service("attrAttrgroupRelationService")
public class AttrAttrgroupRelationServiceImpl extends ServiceImpl<AttrAttrgroupRelationDao, AttrAttrgroupRelationEntity> implements AttrAttrgroupRelationService {

    @Autowired
    AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrAttrgroupRelationEntity> page = this.page(
                new Query<AttrAttrgroupRelationEntity>().getPage(params),
                new QueryWrapper<AttrAttrgroupRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void deleteRelations(AttrGroupRelationVo[] relations) {

        //抽取包装类转成Mapper类型
        List<AttrAttrgroupRelationEntity> relationEntities = Arrays.asList(relations).stream().map(el -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(el, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());


        attrAttrgroupRelationDao.deleteBatchRelation(relationEntities);
    }

    @Override
    public void saveRelations(AttrGroupRelationVo[] relations) {
        //抽取包装类转成Mapper类型
        List<AttrAttrgroupRelationEntity> relationEntities = Arrays.asList(relations).stream().map(el -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(el, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());

        this.saveBatch(relationEntities);
    }

}