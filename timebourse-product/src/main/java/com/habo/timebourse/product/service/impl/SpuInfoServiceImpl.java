package com.habo.timebourse.product.service.impl;

import com.habo.timebourse.product.entity.*;
import com.habo.timebourse.product.feign.CouPonFeignService;
import com.habo.timebourse.product.service.*;
import com.habo.timebourse.product.vo.*;
import com.timebourse.common.to.SkuReductionTo;
import com.timebourse.common.to.SpuBoundTo;
import com.timebourse.common.utils.R;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    SpuInfoDescService spuInfoDescService;

    @Autowired
    SpuImagesService spuImagesService;

    @Autowired
    AttrService attrService;

    @Autowired
    ProductAttrValueService attrValueService;

    @Autowired
    SkuInfoService skuInfoService;

    @Autowired
    SkuImagesService skuImagesService;

    @Autowired
    SkuSaleAttrValueService skuSaleAttrValueService;

    @Autowired
    CouPonFeignService couPonFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSpuInfo(SpuSaveVo spuvo) {
        //1.保存商品基本信息
        SpuInfoEntity infoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuvo,infoEntity);
        infoEntity.setCreateTime(new Date());
        infoEntity.setUpdateTime(new Date());
        this.save(infoEntity);
        //2.保存Spu的描述图片
        List<String> spuvoDecript = spuvo.getDecript();
        if(spuvoDecript != null && spuvoDecript.size() > 0){
            SpuInfoDescEntity descEntity = new SpuInfoDescEntity();
            descEntity.setSpuId(infoEntity.getId());
            descEntity.setDecript(String.join(",",spuvoDecript));
            spuInfoDescService.save(descEntity);
        }
        //3.保存spu的图片集
        List<String> images = spuvo.getImages();
        if(images != null && images.size() > 0){
            List<SpuImagesEntity> imagesEntities = images.stream().map(el -> {
                SpuImagesEntity imagesEntity = new SpuImagesEntity();
                imagesEntity.setSpuId(infoEntity.getId());
                imagesEntity.setImgUrl(el);
                return imagesEntity;
            }).collect(Collectors.toList());
            spuImagesService.saveBatch(imagesEntities);
        }
        //4.保存spu的规格参数
        List<BaseAttrs> baseAttrs = spuvo.getBaseAttrs();
        if(baseAttrs !=null && baseAttrs.size() > 0){
            List<ProductAttrValueEntity> attrValueEntities = baseAttrs.stream().map(el -> {
                ProductAttrValueEntity attrValueEntity = new ProductAttrValueEntity();
                attrValueEntity.setSpuId(infoEntity.getId());
                attrValueEntity.setAttrId(el.getAttrId());
                //属性的名称
                attrValueEntity.setAttrName(attrService.getById(el.getAttrId()).getAttrName());
                attrValueEntity.setAttrSort(0);
                attrValueEntity.setAttrValue(el.getAttrValues());
                attrValueEntity.setQuickShow(el.getShowDesc());
                return attrValueEntity;
            }).collect(Collectors.toList());
            attrValueService.saveBatch(attrValueEntities);
        }
        //5.保存sku的基本信息
        List<Skus> skus = spuvo.getSkus();
        if(skus !=null && skus.size() > 0){
            skus.forEach( el -> {
                //取出默认图片
                String defaultImage = "";
                for (Images image : el.getImages()){
                    if (image.getDefaultImg() == 1){
                        defaultImage = image.getImgUrl();
                    }
                }

                //5.1 保存基本属性
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(el,skuInfoEntity);
                skuInfoEntity.setBrandId(infoEntity.getBrandId());
                skuInfoEntity.setCatalogId(infoEntity.getCatalogId());
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setSpuId(infoEntity.getId());
                skuInfoEntity.setSkuDefaultImg(defaultImage);
                skuInfoService.save(skuInfoEntity);

                Long skuId = skuInfoEntity.getSkuId();

                //5.2 保存图片
                List<SkuImagesEntity> skuImagesEntities = new ArrayList<>();
                List<Images> imagesList = el.getImages();
                if (imagesList != null && imagesList.size() > 0){
                    skuImagesEntities = imagesList.stream().map(img -> {
                        SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                        skuImagesEntity.setSkuId(skuId);
                        skuImagesEntity.setImgUrl(img.getImgUrl());
                        skuImagesEntity.setDefaultImg(img.getDefaultImg());
                        return skuImagesEntity;
                    }).filter(img ->{
                        //过滤无图片地址
                        return !StringUtils.isEmpty(img.getImgUrl());
                    }).collect(Collectors.toList());
                }
                skuImagesService.saveBatch(skuImagesEntities);

                //5.3 保存sku销售属性
                List<Attr> attr = el.getAttr();
                List<SkuSaleAttrValueEntity> attrValueEntities = new ArrayList<>();
                if (attr !=null && attr.size() > 0){
                    attrValueEntities = attr.stream().map(attrr -> {
                        SkuSaleAttrValueEntity skuSaleAttrValueEntity = new SkuSaleAttrValueEntity();
                        BeanUtils.copyProperties(attrr, skuSaleAttrValueEntity);
                        skuSaleAttrValueEntity.setSkuId(skuId);
                        return skuSaleAttrValueEntity;
                    }).collect(Collectors.toList());
                }
                skuSaleAttrValueService.saveBatch(attrValueEntities);

                //5.4远程服务保存sku打折信息
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtils.copyProperties(el,skuReductionTo);
                skuReductionTo.setSkuId(skuId);
                //商品有满减或打折的信息时
                if(skuReductionTo.getFullPrice() .compareTo(BigDecimal.ZERO) == 1 || skuReductionTo.getFullCount() > 0){
                    R r = couPonFeignService.saveSkuReduction(skuReductionTo);
                    if(r.getCode() != 0){
                        log.error("远程服务保存sku打折信息失败");
                    }
                }
            });
        }

        //6.保存spu的积分信息
        Bounds bounds = spuvo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        BeanUtils.copyProperties(bounds,spuBoundTo);
        spuBoundTo.setSpuId(infoEntity.getId());
        R r = couPonFeignService.saveBounds(spuBoundTo);
        if(r.getCode() != 0){
            log.error("保存spu的积分信息失败");
        }


    }

    @Override
    public PageUtils queryParamsPage(Map<String, Object> params) {

        //构建查询条件
        QueryWrapper<SpuInfoEntity> wrapper = new QueryWrapper<>();

        //key为id or name
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.and(el -> {
                el.eq("id",key)
                        .or()
                        .like("spu_name",key);
            });
        }

        //status 上架状态
        String status = (String) params.get("status");
        if(!StringUtils.isEmpty(status)){
            wrapper.eq("publish_status",status);
        }
        //brandId 品牌
        String brandId = (String) params.get("brandId");
        if(!StringUtils.isEmpty(brandId)){
            BigDecimal bigDecimal = new BigDecimal(brandId);
            if(bigDecimal.compareTo(BigDecimal.ZERO) == 1){
                wrapper.eq("brand_id",brandId);
            }
        }
        //catelogId 分类
        String catelogId = (String) params.get("catelogId");
        if(!StringUtils.isEmpty(catelogId)){
            BigDecimal bigDecimal = new BigDecimal(catelogId);
            if(bigDecimal.compareTo(BigDecimal.ZERO) == 1){
                wrapper.eq("catalog_id",catelogId);
            }
        }

        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}