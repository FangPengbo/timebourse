package com.habo.timebourse.product.dao;

import com.habo.timebourse.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 20:08:34
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
