package com.habo.timebourse.order.dao;

import com.habo.timebourse.order.entity.OrderReturnReasonEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退货原因
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:53:45
 */
@Mapper
public interface OrderReturnReasonDao extends BaseMapper<OrderReturnReasonEntity> {
	
}
