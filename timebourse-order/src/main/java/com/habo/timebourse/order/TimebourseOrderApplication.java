package com.habo.timebourse.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.habo.timebourse.order.dao")
@SpringBootApplication
public class TimebourseOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseOrderApplication.class, args);
    }

}
