package com.habo.timebourse.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class TimebourseThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseThirdPartyApplication.class, args);
    }

}
