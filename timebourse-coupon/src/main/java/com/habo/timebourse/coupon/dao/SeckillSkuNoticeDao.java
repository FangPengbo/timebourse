package com.habo.timebourse.coupon.dao;

import com.habo.timebourse.coupon.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:40:35
 */
@Mapper
public interface SeckillSkuNoticeDao extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
