package com.habo.timebourse.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.timebourse.common.to.SkuReductionTo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:40:35
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo skuReductionTo);
}

