package com.habo.timebourse.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.habo.timebourse.coupon.dao")
@SpringBootApplication
public class TimebourseCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseCouponApplication.class, args);
    }

}
