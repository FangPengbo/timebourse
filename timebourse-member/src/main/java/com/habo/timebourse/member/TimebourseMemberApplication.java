package com.habo.timebourse.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.habo.timebourse.member.feign")
@EnableDiscoveryClient
@MapperScan("com.habo.timebourse.member.dao")
@SpringBootApplication
public class TimebourseMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseMemberApplication.class, args);
    }

}
