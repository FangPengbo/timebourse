package com.habo.timebourse.member.feign;

import com.timebourse.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 远程调用Coupon服务接口
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/04/06/13:04
 * @Description:
 */
@FeignClient("timebourse-coupon")
public interface CouponFeignService {

    /**
     * 调用coupon服务的membercoupons方法
     * @return
     */
    @RequestMapping("coupon/coupon/member/list")
    public R membercoupons();

}
