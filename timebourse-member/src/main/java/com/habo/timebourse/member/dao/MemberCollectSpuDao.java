package com.habo.timebourse.member.dao;

import com.habo.timebourse.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:49:26
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
