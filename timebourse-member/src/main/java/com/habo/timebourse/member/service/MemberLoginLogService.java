package com.habo.timebourse.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:49:26
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

