package com.habo.timebourse.ware.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/16/14:08
 * @Description:
 */
@Data
public class PurchaseItemDoneVo {

    private Long itemId;
    private Integer status;
    private String reason;

}
