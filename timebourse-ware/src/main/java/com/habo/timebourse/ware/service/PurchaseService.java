package com.habo.timebourse.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.habo.timebourse.ware.vo.MergeVo;
import com.habo.timebourse.ware.vo.PurchaseDoneVo;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:58:12
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryUnreceive(Map<String, Object> params);

    void merge(MergeVo mergeVo);

    void received(List<Long> ids);

    void done(PurchaseDoneVo doneVo);
}

