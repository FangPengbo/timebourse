package com.habo.timebourse.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableFeignClients(basePackages = "com.habo.timebourse.ware.feign")
@EnableTransactionManagement
@EnableDiscoveryClient
@MapperScan("com.habo.timebourse.ware.dao")
@SpringBootApplication
public class TimebourseWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimebourseWareApplication.class, args);
    }

}
