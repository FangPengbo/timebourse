package com.habo.timebourse.ware.service.impl;

import com.habo.timebourse.ware.entity.PurchaseDetailEntity;
import com.habo.timebourse.ware.service.PurchaseDetailService;
import com.habo.timebourse.ware.service.WareInfoService;
import com.habo.timebourse.ware.service.WareSkuService;
import com.habo.timebourse.ware.vo.MergeVo;
import com.habo.timebourse.ware.vo.PurchaseDoneVo;
import com.habo.timebourse.ware.vo.PurchaseItemDoneVo;
import com.timebourse.common.constant.WareConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.timebourse.common.utils.PageUtils;
import com.timebourse.common.utils.Query;

import com.habo.timebourse.ware.dao.PurchaseDao;
import com.habo.timebourse.ware.entity.PurchaseEntity;
import com.habo.timebourse.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {


    @Autowired
    PurchaseDetailService purchaseDetailService;

    @Autowired
    WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryUnreceive(Map<String, Object> params) {

        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>().eq("status",0).or().eq("status",1)
        );

        return new PageUtils(page);

    }

    @Override
    public void merge(MergeVo mergeVo) {
        Long purchaseId = mergeVo.getPurchaseId();

        //1.合并未存在采购单
        if(mergeVo.getPurchaseId() == null){
            //1.1 创建采购单
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setUpdateTime(new Date());
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATED.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }
        //2.合并已存在采购单
        List<Long> items = mergeVo.getItems();
        List<PurchaseDetailEntity> purchaseDetailEntities = new ArrayList<>();
        if(items !=null && items.size() > 0){
            Long finalPurchaseId = purchaseId;
            purchaseDetailEntities = items.stream().map(el -> {
                PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
                purchaseDetailEntity.setId(el);
                purchaseDetailEntity.setPurchaseId(finalPurchaseId);
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode());
                return purchaseDetailEntity;
            }).collect(Collectors.toList());

        }
        purchaseDetailService.updateBatchById(purchaseDetailEntities);

        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());

        this.updateById(purchaseEntity);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void received(List<Long> ids) {
        //1.确认当前采购单是新建或者已分配
        List<PurchaseEntity> purchaseEntities = ids.stream().map(id -> {
            PurchaseEntity purchaseEntity = this.getById(id);
            return purchaseEntity;
        }).filter(el -> {
            if (el.getStatus() == WareConstant.PurchaseStatusEnum.CREATED.getCode() ||
                    el.getStatus() == WareConstant.PurchaseStatusEnum.ASSIGNED.getCode()) {
                return true;
            }
            return false;
        }).map(el -> {
            el.setStatus(WareConstant.PurchaseStatusEnum.RECEIVE.getCode());
            el.setUpdateTime(new Date());
            return el;
        }).collect(Collectors.toList());
        //2.改变采购单的状态
        this.updateBatchById(purchaseEntities);

        //3.改变采购项的状态
        purchaseEntities.forEach(el -> {
            List<PurchaseDetailEntity> entities = purchaseDetailService.updateStatusDetailByPurchase(el.getId());
            List<PurchaseDetailEntity> collect = entities.stream().map(entity -> {
                PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
                detailEntity.setId(entity.getId());
                detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.BUYING.getCode());
                return detailEntity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(collect);
        });

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void done(PurchaseDoneVo doneVo) {
        Long id = doneVo.getId();
        //1.改变采购项的状态->已完成or 有异常
        Boolean flag = true;
        List<PurchaseItemDoneVo> items = doneVo.getItems();
        List<PurchaseDetailEntity> updates = new ArrayList<>();
        if(items !=null && items.size()> 0){
            for (PurchaseItemDoneVo item : items) {
                PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
                if(item.getStatus() == WareConstant.PurchaseDetailStatusEnum.HASERROR.getCode()){
                    flag = false;
                    detailEntity.setStatus(item.getStatus());
                }else {
                    //增加商品的库存
                    detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FINISH.getCode());
                    PurchaseDetailEntity entity = purchaseDetailService.getById(item.getItemId());
                    wareSkuService.addStock(entity.getSkuId(),entity.getWareId(),entity.getSkuNum());
                }
                detailEntity.setId(item.getItemId());
                updates.add(detailEntity);
            }
            purchaseDetailService.updateBatchById(updates);
        }
        //2.改变采购单的状态->已完成or 有异常



    }

}