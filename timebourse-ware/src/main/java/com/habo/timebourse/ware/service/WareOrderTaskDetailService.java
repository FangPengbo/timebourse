package com.habo.timebourse.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.timebourse.common.utils.PageUtils;
import com.habo.timebourse.ware.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:58:12
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

