package com.habo.timebourse.ware.feign;

import com.timebourse.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 远程调用Product服务
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/16/14:31
 * @Description:
 */
@FeignClient("timebourse-product")
public interface ProductFeignService {

    /**
     * 信息
     */
    @RequestMapping("product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);

}
