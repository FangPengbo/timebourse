package com.habo.timebourse.ware.dao;

import com.habo.timebourse.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangpengbo
 * @email 827097000@qq.com
 * @date 2020-04-05 21:58:12
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
