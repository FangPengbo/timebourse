package com.habo.timebourse.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/05/16/14:09
 * @Description:
 */
@Data
public class PurchaseDoneVo {

    private Long id;
    private List<PurchaseItemDoneVo> items;

}
