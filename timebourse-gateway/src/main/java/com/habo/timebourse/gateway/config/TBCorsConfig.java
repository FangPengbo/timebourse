package com.habo.timebourse.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * 网关统一解决跨域问题
 *
 * @Auther: Fang Pengbo
 * @Date: 2020/04/08/22:14
 * @Description:
 */
@Configuration
public class TBCorsConfig {


    /**
     * 用Spring提供的CorsWebFilter配置类
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //配置跨域
        //允许所有请求头
        corsConfiguration.addAllowedHeader("*");
        //允许所有请求方法
        corsConfiguration.addAllowedMethod("*");
        //允许所有请求源
        corsConfiguration.addAllowedOrigin("*");
        //允许cookie
        corsConfiguration.setAllowCredentials(true);

        //注册跨域的配置
        source.registerCorsConfiguration("/**",corsConfiguration);
        return new CorsWebFilter(source);
    }


}
